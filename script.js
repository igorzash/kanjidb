(function () {
    const kanjiView = document.getElementById('kanjiView');

    async function cacheFirst(kanji) {
        const cached = localStorage.getItem(kanji);

        return cached ? JSON.parse(cached) : await (async function () {
            const response = await fetch(`https://kanjiapi.dev/v1/kanji/${kanji}`);
            const body = await response.json();
            localStorage.setItem(kanji, JSON.stringify(body));
            return body;
        })();
    }

    async function kanjiSelect(kanji) {
        const data = await cacheFirst(kanji);

        let html = `<div class="flexColumn" style="justify-content: between; height: 100%;"><h2>${kanji}</h2><div style="height: 100%"></div>`;
        
        if (data.meanings.length) {
            html += `<div class="box"><h3>英語</h3> ${data.meanings.join(', ')}</div>`;
        }

        if (data.on_readings.length) {
            html += `<div class="box"><h3>音読み</h3> ${data.on_readings.join('、')}</div>`;
        }
        
        if (data.kun_readings.length) {
            html += `<div class="box"><h3>居のみ</h3> ${data.kun_readings.join('、')}</div>`;
        }

        kanjiView.innerHTML = html + '</div>';
    }

    const canvas = 'canvas';
    const candidateList = document.getElementById('candidates');

    KanjiCanvas.init(canvas);

    function recognizeTrigger() {
        let result = KanjiCanvas.recognize(canvas);

        result = result.split(' ').filter(Boolean);

        result.reverse();

        let first5 = result.slice(0, 5);
        let last5 = result.slice(5);

        last5.reverse();
        first5.reverse();

        result = first5.concat(last5);

        candidateList.innerHTML = '';

        result.forEach(kanji => {
            const button = document.createElement('button');

            button.innerText = kanji;

            button.addEventListener('click', function () {
                kanjiSelect(kanji);
            });

            candidateList.appendChild(button);
        });
    }

    document.getElementById(canvas).addEventListener('click', recognizeTrigger);

    document.getElementById('erase-button').addEventListener('click', function () {
        KanjiCanvas.erase(canvas);
        recognizeTrigger();
    });

    document.getElementById('undo-button').addEventListener('click', function () {
        KanjiCanvas.deleteLast(canvas);
        recognizeTrigger();
    });

    recognizeTrigger();
})();